# deliveryheroselenium

Technical task for Senior QA Engineer
Results of the technical task should be provided in a form of git repository (github/bitbucket).
Please think about the structure of your code, think about readability, scalability and avoid to
use antipatterns.

1. Create a simple scenario using Selenium (framework by your choice) for the following
requirement statement:
Customer visits Amazon website to purchase items by adding them to their shopping
cart:
1. Open amazon homepage.
2. Search for any product (verify search input field and search button).
3. Verify found results (please explain fragility of tests here and how we can fix it if
we own the product).

Testing applications at the UI level is often fragile as any change in the application markup can effect the locating of
elements on a page. In order to minimize the impact of UI changes and the effect on automated UI tests developers can
add an attribute to each result that does not change. With a static attribute on each result the effect of changes in the
UI would be minimized. As well as this, testing results could be done at the api level. API tests are faster and more
stable than UI tests. API tests could verify the results that are returned from a search and would not be effected by
changes in the UI.




2. Create a simple scenario using Selenium (framework by your choice) for the Login
feature on https://www.pizza.de . There is no requirement statement on purpose, you
need to think about different scenarios.
If you find any bugs or inconsistencies please document them and report back.

