package amazon;

import base.TestBase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.AmazonHomePage;
import pages.AmazonResultsPage;

public class AmazonTests extends TestBase {

    @BeforeMethod
    public void setup(){
        driver.navigate().to("http://www.amazon.com");
    }

    /*
        Create a simple scenario using Selenium (framework by your choice) for the following
        requirement statement:
        Customer visits Amazon website to purchase items by adding them to their shopping
        cart:
        1. Open amazon homepage.
        2. Search for any product (verify search input field and search button).
        3. Verify found results (please explain fragility of tests here and how we can fix it if
        we own the product).
    */

    @Test
    public void search(){
        new AmazonHomePage(driver).search("Esp guitars");
        AmazonResultsPage resultsPage = new AmazonResultsPage(driver);
        Assert.assertTrue(resultsPage.allResultsContain("Esp"));
    }
}
