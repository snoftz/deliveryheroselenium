package pizza;

import base.TestBase;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.PizzaHomePage;

public class PizzaTests extends TestBase {

    @BeforeMethod
    public void setup(){
        driver.navigate().to("https://pizza.de");
    }


    /*
        2. Create a simple scenario using Selenium (framework by your choice) for the Login
        feature on https://www.pizza.de . There is no requirement statement on purpose, you
        need to think about different scenarios.
        If you find any bugs or inconsistencies please document them and report back.
     */

    @Test
    public void loginWithValidCredentials(){
        PizzaHomePage page = new PizzaHomePage(driver);
        page.login("test@email.com", "password");
        Assert.assertTrue(page.isLoggedIn(), "User failed to login");
    }

    @Test
    public void attemptLoginWithInvalidCredentials(){
        PizzaHomePage page = new PizzaHomePage(driver);
        page.login("test", "test");
        Assert.assertTrue(page.getLoginFormErrorMessage().isDisplayed(), "Error message not displayed");
    }

    @Test
    public void attemptLoginWithNoUsernamePassword(){
        PizzaHomePage page = new PizzaHomePage(driver);
        page.login("", "");
        Assert.assertTrue(page.getUsernameErrorMessage().isDisplayed(), "Username error message not displayed");
        Assert.assertTrue(page.getPasswordErrorMessage().isDisplayed(), "Password error message not displayed");
    }

    @Test
    public void attemptLoginWithNoPassword(){
        PizzaHomePage page = new PizzaHomePage(driver);
        page.login("test", "");
        Assert.assertTrue(page.getPasswordErrorMessage().isDisplayed(), "Password error message not displayed");
    }

    @Test
    public void attemptLoginWithNoUsername(){
        PizzaHomePage page = new PizzaHomePage(driver);
        page.login("", "pass");
        Assert.assertTrue(page.getUsernameErrorMessage().isDisplayed(), "Username error message not displayed");
    }

    @Test
    public void usernameLabelAppearsAfterTyping(){
        PizzaHomePage page = new PizzaHomePage(driver);
        page.getHomeLoginButton().click();
        page.getUsernameTextbox().sendKeys("a");
        Assert.assertTrue(page.getUsernameLabel().isDisplayed(), "Username label not displayed");
    }

    @Test
    public void passwordLabelAppearsAfterTyping(){
        PizzaHomePage page = new PizzaHomePage(driver);
        page.getHomeLoginButton().click();
        page.getPasswordTextbox().sendKeys("a");
        Assert.assertTrue(page.getPasswordLabel().isDisplayed(), "Password label not displayed");
    }
}
