package util;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

public class Wait {

    public static void waitForElementVisible(WebDriver driver, By by){
        FluentWait<WebDriver> wait = new FluentWait<>(driver);
        wait.withTimeout(java.time.Duration.ofSeconds(3))
                .ignoring(StaleElementReferenceException.class)
                .ignoring(ElementNotVisibleException.class)
                .ignoring(NoSuchElementException.class)
                .pollingEvery(java.time.Duration.ofMillis(250))
                .until(ExpectedConditions.visibilityOfElementLocated(by));
    }

}
