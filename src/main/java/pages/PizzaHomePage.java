package pages;

import org.openqa.selenium.*;
import util.Wait;

public class PizzaHomePage {

    private WebDriver driver;

    public PizzaHomePage(WebDriver webDriver){
        driver = webDriver;
    }

    private WebElement homeLoginButton() { return driver.findElement(By.cssSelector("button[data-qa=login-button]")); }
    private WebElement formLoginButton() { return driver.findElement(By.cssSelector("button[data-qa=login-form-button-submit]")); }
    private WebElement loginFormErrorMessage() { return driver.findElement(By.cssSelector("span[data-qa=user-form-message-error]")); }
    private WebElement passwordErrorMessage() { return driver.findElement(By.xpath("//input[@name='password']/../div[@class='input__error']")); }
    private WebElement passwordLabel() {
        return driver.findElement(By.xpath("//form[@data-qa='login-form']/div/div/div/div/input[@name='password']/../../label[@class='input__label']"));
    }
    private WebElement passwordTextbox() { return driver.findElement(By.cssSelector("form[data-qa=login-form] input[name=password]")); }
    private WebElement usernameErrorMessage() { return driver.findElement(By.xpath("//input[@name='username']/../div[@class='input__error']")); }
    private WebElement usernameLabel() { return driver.findElement(By.xpath("//input[@name='username']/../../label[@class='input__label']")); }
    private WebElement usernameTextbox() { return driver.findElement(By.name("username")); }

    public WebElement getHomeLoginButton(){
        return homeLoginButton();
    }

    public WebElement getLoginFormErrorMessage(){
        Wait.waitForElementVisible(driver, By.cssSelector("span[data-qa=user-form-message-error]"));
        return loginFormErrorMessage();
    }

    public WebElement getPasswordErrorMessage(){
        return passwordErrorMessage();
    }

    public WebElement getPasswordLabel(){
        return passwordLabel();
    }

    public WebElement getPasswordTextbox(){
        return passwordTextbox();
    }

    public WebElement getUsernameErrorMessage(){
        return usernameErrorMessage();
    }

    public WebElement getUsernameLabel(){
        return usernameLabel();
    }

    public WebElement getUsernameTextbox(){
        return usernameTextbox();
    }

    public void login(String username, String password){
        homeLoginButton().click();
        usernameTextbox().sendKeys(username);
        passwordTextbox().sendKeys(password);
        formLoginButton().click();
    }

    public boolean isLoggedIn(){
        try{
            Wait.waitForElementVisible(driver, By.cssSelector("button[data-qa=logout-button]"));
        }catch (Exception e){
            return false;
        }
        return true;
    }
}
