package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AmazonHomePage {

    private WebDriver driver;

    public AmazonHomePage(WebDriver webDriver){
        driver = webDriver;
    }

    private WebElement searchButton() {
        return driver.findElement(By.cssSelector("div.nav-search-submit.nav-sprite>input.nav-input"));
    }
    private WebElement searchTextBox() { return driver.findElement(By.cssSelector("input#twotabsearchtextbox")); }

    public void search(String text){
        searchTextBox().sendKeys(text);
        searchButton().click();
    }
}
