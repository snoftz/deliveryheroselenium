package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class AmazonResultsPage {

    private WebDriver driver;

    public AmazonResultsPage(WebDriver webDriver){
        driver = webDriver;
    }

    private List<WebElement> searchResults() {
        return driver.findElements(By.cssSelector("li[class='s-result-item celwidget  '] div.a-fixed-left-grid-col.a-col-right"));
    }

    public boolean allResultsContain(String text){
        for (WebElement ele: searchResults()) {
            if(!ele.getText().toLowerCase().contains(text.toLowerCase())) {
                return false;
            }
        }
        return true;
    }
}
